class DOMNodeCollection {
  constructor(HTMLElements) {
    this.elements = HTMLElements;
  }

  html (string) {

    if (string === undefined) {
      return this.elements[0].innerHTML;
    }

    this.elements.forEach ((element) => {
      element.innerHTML = string;
    });

  }

  empty() {
    this.elements.forEach ((element) => {
      // element.innerHTML = string;
      while (element.firstChild) {
        element.removeChild(element.firstChild);
      }
    });
  }
  indexDOMNODE(num) {
    return new DOMNodeCollection(this.elements[num]);
  }
  index(num) {
    return this.elements[num];
  }
  length() {
    return this.elements.length;
  }

  append (elementToAppend) {
    if (elementToAppend instanceof DOMNodeCollection) {
      for (let i =0; i < this.elements.length; i++) {
        for (let j = 0; j < elementToAppend.elements.length; j++) {
          this.elements[i].appendChild(elementToAppend.elements[j]);
        }
      }
    } else if (elementToAppend.constructor.name === "String"){
      for (let i = 0; i < this.elements.length; i++) {
        let element = document.createElement(elementToAppend);
        this.elements[i].appendChild(element);
      }

    } else {
      for (let i = 0; i < this.elements.length; i++) {
        this.elements[i].appendChild(elementToAppend);
      }
    }
  }

  attr(name, value) {
    let got;
    this.elements.forEach((element) => {
      if (value !== undefined) {
        element.setAttribute(name, value);
      } else {
        // debugger
        got = element.getAttribute(name);
      }
    });
    return got;
  }

  addClass(className) {
    for (let i = 0; i < this.elements.length; i++) {
      const currentClass = $l(this.elements[i]).attr("class");
      if (currentClass !== null) {
        $l(this.elements[i]).attr("class", `${currentClass} ${className}`);
      } else {
        $l(this.elements[i]).attr("class", `${className}`);
      }
    }
  }

  removeClass(className) {
    for (let i = 0; i < this.elements.length; i++) {
      const currentClass = $l(this.elements[i]).attr("class");
      const indexStart = currentClass.indexOf(className);
      if (indexStart !== -1){
        const left = currentClass.slice(0, indexStart);
        const right = currentClass.slice(indexStart + className.length);
        $l(this.elements[i]).attr("class", left+right);
      }

      if (/^\s+$/.test($l(this.elements[i]).attr("class"))) {
        this.elements[i].removeAttribute("class");
      }
    }
  }

  children() {
    let allChildren = [];
    for (let i = 0; i < this.elements.length; i++) {
      const someChildren = this.elements[i].children;
      allChildren = allChildren.concat(Array.from(someChildren));
    }

    return new DOMNodeCollection(allChildren);
  }



  parent() {
    const allParents = [];
    for (let i = 0; i < this.elements.length; i++) {
      allParents.push(this.elements[i].parentNode);
    }

    return new DOMNodeCollection(allParents);
  }

  find(selector) {
    let foundThings = [];
    for (let i = 0; i < this.elements.length; i++) {
      foundThings = foundThings.concat(Array.from(this.elements[i].querySelectorAll(selector)));
    }
    return new DOMNodeCollection(foundThings);
  }

  remove() {
    for (let i = 0; i < this.elements.length; i++) {
      this.elements[i].parentNode.removeChild(this.elements[i]);
    }

  }
  on(type, callback) {
    for (let i = 0; i < this.elements.length; i++) {
      this.elements[i].addEventListener(type, callback);
    }

  }
  off(type, callback) {
    for (let i = 0; i < this.elements.length; i++) {
      this.elements[i].removeEventListener(type, callback);
    }

  }
}

module.exports = DOMNodeCollection;
