const DOMNodeCollection = require('./dom_node_collection.js');

let ready = false;
const functions = [];

$l = function (selector) {

  if (selector instanceof Function) {
    // debugger

    if (ready) {
      selector();
    } else {
      functions.push(selector);
    }

  } else if (selector instanceof HTMLElement) {
    return new DOMNodeCollection([selector]);
  } else if (selector.constructor.name === "HTMLCollection"){
    const nodeList = Array.from(document.querySelectorAll(selector));
    return new DOMNodeCollection(nodeList);
  }

};

const executeFunctions = function() {
  ready = true;
  for (let i = 0; i < functions.length; i++) {
    functions[i]();
  }
};

window.$l = $l;
document.addEventListener("DOMContentLoaded", executeFunctions);

$l.prototype.extend = function () {
  let args = Array.from(arguments);
  let newObj = {};
  for (let i = 0; i < args.length; i++) {
    for (let attrname in args[i]) { newObj[attrname] = args[i][attrname]; }
  }
  return newObj;
};

$l.prototype.ajax = function (options) {
  let defaultOptions = {
    // data: "abc",
    dataType: 'json',
    method: 'GET',
    success: console.log("success!"),
    error: console.log("error"),
    url: 'http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=bcb83c4b54aee8418983c2aff3073b3b'
  };

  const newObj = this.extend(defaultOptions, options);
  //step 1 - create xhr object
  const xhr = new XMLHttpRequest();

// step 2 - specify path and verb
  xhr.open(newObj.method, newObj.url);

// step 3 - register a callback
  xhr.onload = function () {
    console.log(xhr.status); // for status info
    console.log(xhr.responseType); //the type of data that was returned
    console.log(xhr.response); //the actual response. For json api calls, this will be a json string
  };

  // step 4 - send off the request with optional data
  xhr.send(newObj);
};
