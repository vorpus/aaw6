/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	const DOMNodeCollection = __webpack_require__(1);

	let ready = false;
	const functions = [];

	$l = function (selector) {

	  if (selector instanceof Function) {
	    // debugger

	    if (ready) {
	      selector();
	    } else {
	      functions.push(selector);
	    }

	  } else if (selector instanceof HTMLElement) {
	    return new DOMNodeCollection([selector]);
	  } else if (selector.constructor.name === "HTMLCollection"){
	    const nodeList = Array.from(document.querySelectorAll(selector));
	    return new DOMNodeCollection(nodeList);
	  }

	};

	const executeFunctions = function() {
	  ready = true;
	  for (let i = 0; i < functions.length; i++) {
	    functions[i]();
	  }
	};

	window.$l = $l;
	document.addEventListener("DOMContentLoaded", executeFunctions);

	$l.prototype.extend = function () {
	  let args = Array.from(arguments);
	  let newObj = {};
	  for (let i = 0; i < args.length; i++) {
	    for (let attrname in args[i]) { newObj[attrname] = args[i][attrname]; }
	  }
	  return newObj;
	};

	$l.prototype.ajax = function (options) {
	  let defaultOptions = {
	    // data: "abc",
	    dataType: 'json',
	    method: 'GET',
	    success: console.log("success!"),
	    error: console.log("error"),
	    url: 'http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=bcb83c4b54aee8418983c2aff3073b3b'
	  };

	  const newObj = this.extend(defaultOptions, options);
	  //step 1 - create xhr object
	  const xhr = new XMLHttpRequest();

	// step 2 - specify path and verb
	  xhr.open(newObj.method, newObj.url);

	// step 3 - register a callback
	  xhr.onload = function () {
	    console.log(xhr.status); // for status info
	    console.log(xhr.responseType); //the type of data that was returned
	    console.log(xhr.response); //the actual response. For json api calls, this will be a json string
	  };

	  // step 4 - send off the request with optional data
	  xhr.send(newObj);
	};


/***/ },
/* 1 */
/***/ function(module, exports) {

	class DOMNodeCollection {
	  constructor(HTMLElements) {
	    this.elements = HTMLElements;
	  }

	  html (string) {

	    if (string === undefined) {
	      return this.elements[0].innerHTML;
	    }

	    this.elements.forEach ((element) => {
	      element.innerHTML = string;
	    });

	  }

	  empty() {
	    this.elements.forEach ((element) => {
	      // element.innerHTML = string;
	      while (element.firstChild) {
	        element.removeChild(element.firstChild);
	      }
	    });
	  }
	  indexDOMNODE(num) {
	    return new DOMNodeCollection(this.elements[num]);
	  }
	  index(num) {
	    return this.elements[num];
	  }
	  length() {
	    return this.elements.length;
	  }

	  append (elementToAppend) {
	    if (elementToAppend instanceof DOMNodeCollection) {
	      for (let i =0; i < this.elements.length; i++) {
	        for (let j = 0; j < elementToAppend.elements.length; j++) {
	          this.elements[i].appendChild(elementToAppend.elements[j]);
	        }
	      }
	    } else if (elementToAppend.constructor.name === "String"){
	      for (let i = 0; i < this.elements.length; i++) {
	        let element = document.createElement(elementToAppend);
	        this.elements[i].appendChild(element);
	      }

	    } else {
	      for (let i = 0; i < this.elements.length; i++) {
	        this.elements[i].appendChild(elementToAppend);
	      }
	    }
	  }

	  attr(name, value) {
	    let got;
	    this.elements.forEach((element) => {
	      if (value !== undefined) {
	        element.setAttribute(name, value);
	      } else {
	        // debugger
	        got = element.getAttribute(name);
	      }
	    });
	    return got;
	  }

	  addClass(className) {
	    for (let i = 0; i < this.elements.length; i++) {
	      const currentClass = $l(this.elements[i]).attr("class");
	      if (currentClass !== null) {
	        $l(this.elements[i]).attr("class", `${currentClass} ${className}`);
	      } else {
	        $l(this.elements[i]).attr("class", `${className}`);
	      }
	    }
	  }

	  removeClass(className) {
	    for (let i = 0; i < this.elements.length; i++) {
	      const currentClass = $l(this.elements[i]).attr("class");
	      const indexStart = currentClass.indexOf(className);
	      if (indexStart !== -1){
	        const left = currentClass.slice(0, indexStart);
	        const right = currentClass.slice(indexStart + className.length);
	        $l(this.elements[i]).attr("class", left+right);
	      }

	      if (/^\s+$/.test($l(this.elements[i]).attr("class"))) {
	        this.elements[i].removeAttribute("class");
	      }
	    }
	  }

	  children() {
	    let allChildren = [];
	    for (let i = 0; i < this.elements.length; i++) {
	      const someChildren = this.elements[i].children;
	      allChildren = allChildren.concat(Array.from(someChildren));
	    }

	    return new DOMNodeCollection(allChildren);
	  }



	  parent() {
	    const allParents = [];
	    for (let i = 0; i < this.elements.length; i++) {
	      allParents.push(this.elements[i].parentNode);
	    }

	    return new DOMNodeCollection(allParents);
	  }

	  find(selector) {
	    let foundThings = [];
	    for (let i = 0; i < this.elements.length; i++) {
	      foundThings = foundThings.concat(Array.from(this.elements[i].querySelectorAll(selector)));
	    }
	    return new DOMNodeCollection(foundThings);
	  }

	  remove() {
	    for (let i = 0; i < this.elements.length; i++) {
	      this.elements[i].parentNode.removeChild(this.elements[i]);
	    }

	  }
	  on(type, callback) {
	    for (let i = 0; i < this.elements.length; i++) {
	      this.elements[i].addEventListener(type, callback);
	    }

	  }
	  off(type, callback) {
	    for (let i = 0; i < this.elements.length; i++) {
	      this.elements[i].removeEventListener(type, callback);
	    }

	  }
	}

	module.exports = DOMNodeCollection;


/***/ }
/******/ ]);