/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	const FollowToggle = __webpack_require__(1);
	const UsersSearch = __webpack_require__(3);
	const TweetCompose = __webpack_require__(4);
	
	$(() => {
	  $('button.follow-toggle').each((_,dom) => {
	    new FollowToggle(dom);
	  });
	
	  $('.searchbar').each((_,dom) => {
	    new UsersSearch(dom);
	  });
	
	  $('.tweet-compose').each((_,dom) => {
	    new TweetCompose(dom);
	  });
	});


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	const APIUtil = __webpack_require__(2);
	
	class FollowToggle {
	  constructor(el) {
	    this.$el = $(el);
	    this.userId = this.$el.data("user-id");
	    this.followState = this.$el.data("user-follow-state");
	    this.pendingState = undefined;
	    this.render();
	    this.handleClick();
	  }
	
	  render () {
	    if (this.pendingState) {
	      this.$el.text(this.pendingState);
	      this.$el.prop("disabled", true);
	    } else {
	      const followState = this.followState ? 'Unfollow!' : 'Follow!';
	      this.$el.text(followState);
	    }
	  }
	
	  handleClick () {
	
	    const success = () => {
	      this.pendingState = undefined;
	      this.followState = !this.followState;
	      this.$el.prop("disabled", false);
	      this.render();
	    };
	
	    this.$el.on("click", (e) => {
	      e.preventDefault();
	
	      if (this.followState) {
	        this.pendingState = 'unfollowing';
	        this.render();
	        APIUtil.unfollowUser(this.userId, success);
	      } else {
	        this.pendingState = 'following';
	        this.render();
	        APIUtil.followUser(this.userId, success);
	      }
	    });
	  }
	
	
	
	}
	
	module.exports = FollowToggle;


/***/ },
/* 2 */
/***/ function(module, exports) {

	const APIUtil = {
	  followUser(id, success) {
	    // ...
	    $.ajax({
	      url: `/users/${id}/follow.json`,
	      type: 'POST',
	      success
	    });
	  },
	
	  unfollowUser(id, success) {
	    // ...
	    $.ajax({
	      url: `/users/${id}/follow.json`,
	      type: 'DELETE',
	      success
	    });
	  },
	
	  searchUsers(queryVal, success) {
	    $.ajax({
	      url: `/users/search`,
	      method: "GET",
	      dataType: "json",
	      data: {query: queryVal},
	      success
	    });
	  },
	
	  createTweet(tweetBody) {
	    return $.ajax({
	      url: `/tweets`,
	      method: "POST",
	      dataType: "json",
	      data: tweetBody,
	    });
	  },
	
	  error() {
	    console.log(arguments);
	  }
	};
	
	module.exports = APIUtil;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	const APIUtil = __webpack_require__(2);
	
	class UsersSearch {
	  constructor(el) {
	    this.$el = $(el);
	    this.$input = $(".searchbar");
	    this.$ul = $(".users");
	    this.handleInput();
	  }
	
	  renderResults() {
	    this.$ul.empty();
	
	  }
	
	  handleInput() {
	    this.$input.on("input", (e) => {
	      const success = (data) => {
	        this.renderResults();
	
	        data.forEach((datum) => {
	          let newLi = $("<li>");
	          let newA = $("<a>");
	          newA.text(datum.username);
	          newA.attr("href", `/users/${datum.id}`);
	          newLi.append(newA);
	          this.$ul.append(newLi);
	        });
	      };
	
	      let searchValue = e.currentTarget.value;
	      APIUtil.searchUsers(searchValue, success);
	    });
	  }
	}
	
	module.exports = UsersSearch;


/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	const APIUtil = __webpack_require__(2);
	
	class TweetCompose {
	  constructor(el) {
	    this.$el = $(el);
	    this.$el.on("submit", (e) => {
	      e.preventDefault();
	
	      const tweetSerial = this.$el.serialize();
	      const success = (data) => {
	        this.$el.find(":input").attr("disabled", false);
	        const newLi = $("<li>");
	        const newAUser = $("<a>");
	        newAUser.attr("href", `/users/${data.user_id}`);
	        newAUser.append(`${data.user.username}`);
	        newLi.append(`${data.content} -- `);
	        newLi.append(newAUser);
	        newLi.append(` -- ${data.created_at}`);
	
	        if (data.mentions.length > 0) {
	          const newUl = $("<ul>");
	          const newUlLi = $("<li>");
	          const newUlLiA = $("<a>");
	          newUlLiA.attr("href", `/users/${data.mentions[0].user_id}`);
	          newUlLiA.append(`${data.mentions[0].user.username}`);
	          newUlLi.append(newUlLiA);
	          newUl.append(newUlLi);
	          newLi.append(newUl);
	        }
	        $("#feed").prepend(newLi);
	        
	        this.clearInput();
	      };
	
	      this.$el.find(":input").prop("disabled", true);
	      APIUtil.createTweet(tweetSerial).then(tweet => success(tweet));
	    });
	  }
	
	  clearInput() {
	    this.$el.each(function() {
	      this.reset();
	    });
	  }
	
	}
	
	module.exports = TweetCompose;


/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map