const APIUtil = require('./api_util.js');

class FollowToggle {
  constructor(el) {
    this.$el = $(el);
    this.userId = this.$el.data("user-id");
    this.followState = this.$el.data("user-follow-state");
    this.pendingState = undefined;
    this.render();
    this.handleClick();
  }

  render () {
    if (this.pendingState) {
      this.$el.text(this.pendingState);
      this.$el.prop("disabled", true);
    } else {
      const followState = this.followState ? 'Unfollow!' : 'Follow!';
      this.$el.text(followState);
    }
  }

  handleClick () {

    const success = () => {
      this.pendingState = undefined;
      this.followState = !this.followState;
      this.$el.prop("disabled", false);
      this.render();
    };

    this.$el.on("click", (e) => {
      e.preventDefault();

      if (this.followState) {
        this.pendingState = 'unfollowing';
        this.render();
        APIUtil.unfollowUser(this.userId, success);
      } else {
        this.pendingState = 'following';
        this.render();
        APIUtil.followUser(this.userId, success);
      }
    });
  }



}

module.exports = FollowToggle;
