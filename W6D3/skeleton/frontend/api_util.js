const APIUtil = {
  followUser(id, success) {
    // ...
    $.ajax({
      url: `/users/${id}/follow.json`,
      type: 'POST',
      success
    });
  },

  unfollowUser(id, success) {
    // ...
    $.ajax({
      url: `/users/${id}/follow.json`,
      type: 'DELETE',
      success
    });
  },

  searchUsers(queryVal, success) {
    $.ajax({
      url: `/users/search`,
      method: "GET",
      dataType: "json",
      data: {query: queryVal},
      success
    });
  },

  createTweet(tweetBody) {
    return $.ajax({
      url: `/tweets`,
      method: "POST",
      dataType: "json",
      data: tweetBody,
    });
  },

  error() {
    console.log(arguments);
  }
};

module.exports = APIUtil;
