const APIUtil = require("./api_util.js");

class TweetCompose {
  constructor(el) {
    this.$el = $(el);
    this.$el.on("submit", (e) => {
      e.preventDefault();

      const tweetSerial = this.$el.serialize();
      const success = (data) => {
        this.$el.find(":input").attr("disabled", false);
        const newLi = $("<li>");
        const newAUser = $("<a>");
        newAUser.attr("href", `/users/${data.user_id}`);
        newAUser.append(`${data.user.username}`);
        newLi.append(`${data.content} -- `);
        newLi.append(newAUser);
        newLi.append(` -- ${data.created_at}`);

        if (data.mentions.length > 0) {
          const newUl = $("<ul>");
          const newUlLi = $("<li>");
          const newUlLiA = $("<a>");
          newUlLiA.attr("href", `/users/${data.mentions[0].user_id}`);
          newUlLiA.append(`${data.mentions[0].user.username}`);
          newUlLi.append(newUlLiA);
          newUl.append(newUlLi);
          newLi.append(newUl);
        }
        $("#feed").prepend(newLi);
        
        this.clearInput();
      };

      this.$el.find(":input").prop("disabled", true);
      APIUtil.createTweet(tweetSerial).then(tweet => success(tweet));
    });
  }

  clearInput() {
    this.$el.each(function() {
      this.reset();
    });
  }

}

module.exports = TweetCompose;
