const APIUtil = require("./api_util.js");

class UsersSearch {
  constructor(el) {
    this.$el = $(el);
    this.$input = $(".searchbar");
    this.$ul = $(".users");
    this.handleInput();
  }

  renderResults() {
    this.$ul.empty();

  }

  handleInput() {
    this.$input.on("input", (e) => {
      const success = (data) => {
        this.renderResults();

        data.forEach((datum) => {
          let newLi = $("<li>");
          let newA = $("<a>");
          newA.text(datum.username);
          newA.attr("href", `/users/${datum.id}`);
          newLi.append(newA);
          this.$ul.append(newLi);
        });
      };

      let searchValue = e.currentTarget.value;
      APIUtil.searchUsers(searchValue, success);
    });
  }
}

module.exports = UsersSearch;
