const FollowToggle = require('./follow_toggle.js');
const UsersSearch = require('./user_search.js');
const TweetCompose = require('./tweet_compose.js');

$(() => {
  $('button.follow-toggle').each((_,dom) => {
    new FollowToggle(dom);
  });

  $('.searchbar').each((_,dom) => {
    new UsersSearch(dom);
  });

  $('.tweet-compose').each((_,dom) => {
    new TweetCompose(dom);
  });
});
