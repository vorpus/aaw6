class View {
  constructor (game, $view) {
    this.game = game;
    this.$view = $view;
    this.setupTowers();
    this.render();
    this.bindEvents();
    this.currentMove = [];
  }

  setupTowers () {
    // let $startingLi = $('<li class="one"></li><li class="two"></li><li class="three"></li>');
    for (let i = 0; i < 3; i++) {
      this.$view.append($(`<ul class="group t${i}"></ul>`));
    }
    // $($('ul')[0]).append($startingLi);
  }

  wonEffects () {
    $('body').css('background-color', `#${Math.floor(Math.random()*16777215).toString(16)}`);
    $('li').toggleClass('spin');
    console.log("ur a winner");
  }

  bindEvents () {
    $('ul').click((e)=> {
      let selectedTower = parseInt($(e.currentTarget).attr("class")[7]);
      if (this.currentMove.length < 1) {
        this.currentMove.push(selectedTower);
        $(e.currentTarget).children().last().toggleClass('selectedTower');
      } else {
        if (this.game.isValidMove(this.currentMove[0], selectedTower)) {
          this.game.move(this.currentMove[0], selectedTower);
          this.currentMove = [];
          this.render();
          if (this.game.isWon()) {
            $('footer').append('<marquee><p>Congratulations! You win!</p></marquee>');
            window.setInterval(() => {this.wonEffects();}, 1000);
          }
        } else {
          console.log("invalid move");
          this.currentMove = [];
        }
        $('.selectedTower').toggleClass('selectedTower');
      }
    });
  }

  render() {
    $('ul').empty();
    console.log(this.game.towers[0]);

    for (let i = 0; i < 3; i++) {
      let backendTower = this.game.towers[i];
      let frontendTower = $($('ul')[i]);
      backendTower.forEach((t) => {
        let $li = $(`<li class='d${t}'></li>`);
        frontendTower.prepend($li);
      });
      while (frontendTower.children().length < 3) {
        let $li = $(`<li class='d0'></li>`);
        frontendTower.prepend($li);
      }
      let $bar = $(`<li class='bar'></li>`);
      frontendTower.append($bar);
    }
  }

  getMove() {

  }
}

module.exports = View;
