/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	const HanoiGame = __webpack_require__(1);
	const HanoiView = __webpack_require__(2);
	
	$( () => {
	  const rootEl = $('.hanoi');
	  const game = new HanoiGame();
	  new HanoiView(game, rootEl);
	});


/***/ },
/* 1 */
/***/ function(module, exports) {

	class Game {
	  constructor() {
	    this.towers = [[3, 2, 1], [], []];
	  }
	
	  isValidMove(startTowerIdx, endTowerIdx) {
	      const startTower = this.towers[startTowerIdx];
	      const endTower = this.towers[endTowerIdx];
	
	      if (startTower.length === 0) {
	        return false;
	      } else if (endTower.length == 0) {
	        return true;
	      } else {
	        const topStartDisc = startTower[startTower.length - 1];
	        const topEndDisc = endTower[endTower.length - 1];
	        return topStartDisc < topEndDisc;
	      }
	  }
	
	  isWon() {
	      // move all the discs to the last or second tower
	      return (this.towers[2].length == 3) || (this.towers[1].length == 3);
	  }
	
	  move(startTowerIdx, endTowerIdx) {
	      if (this.isValidMove(startTowerIdx, endTowerIdx)) {
	        this.towers[endTowerIdx].push(this.towers[startTowerIdx].pop());
	        return true;
	      } else {
	        return false;
	      }
	  }
	
	  print() {
	      console.log(JSON.stringify(this.towers));
	  }
	
	  promptMove(reader, callback) {
	      this.print();
	      reader.question("Enter a starting tower: ", start => {
	        const startTowerIdx = parseInt(start);
	        reader.question("Enter an ending tower: ", end => {
	          const endTowerIdx = parseInt(end);
	          callback(startTowerIdx, endTowerIdx)
	        });
	      });
	  }
	
	  run(reader, gameCompletionCallback) {
	      this.promptMove(reader, (startTowerIdx, endTowerIdx) => {
	        if (!this.move(startTowerIdx, endTowerIdx)) {
	          console.log("Invalid move!");
	        }
	
	        if (!this.isWon()) {
	          // Continue to play!
	          this.run(reader, gameCompletionCallback);
	        } else {
	          this.print();
	          console.log("You win!");
	          gameCompletionCallback();
	        }
	      });
	  }
	}
	
	module.exports = Game;


/***/ },
/* 2 */
/***/ function(module, exports) {

	class View {
	  constructor (game, $view) {
	    this.game = game;
	    this.$view = $view;
	    this.setupTowers();
	    this.render();
	    this.bindEvents();
	    this.currentMove = [];
	  }
	
	  setupTowers () {
	    // let $startingLi = $('<li class="one"></li><li class="two"></li><li class="three"></li>');
	    for (let i = 0; i < 3; i++) {
	      this.$view.append($(`<ul class="group t${i}"></ul>`));
	    }
	    // $($('ul')[0]).append($startingLi);
	  }
	
	  wonEffects () {
	    $('body').css('background-color', `#${Math.floor(Math.random()*16777215).toString(16)}`);
	    $('li').toggleClass('spin');
	    console.log("ur a winner");
	  }
	
	  bindEvents () {
	    $('ul').click((e)=> {
	      let selectedTower = parseInt($(e.currentTarget).attr("class")[7]);
	      if (this.currentMove.length < 1) {
	        this.currentMove.push(selectedTower);
	        $(e.currentTarget).children().last().toggleClass('selectedTower');
	      } else {
	        if (this.game.isValidMove(this.currentMove[0], selectedTower)) {
	          this.game.move(this.currentMove[0], selectedTower);
	          this.currentMove = [];
	          this.render();
	          if (this.game.isWon()) {
	            $('footer').append('<marquee><p>Congratulations! You win!</p></marquee>');
	            window.setInterval(() => {this.wonEffects();}, 1000);
	          }
	        } else {
	          console.log("invalid move");
	          this.currentMove = [];
	        }
	        $('.selectedTower').toggleClass('selectedTower');
	      }
	    });
	  }
	
	  render() {
	    $('ul').empty();
	    console.log(this.game.towers[0]);
	
	    for (let i = 0; i < 3; i++) {
	      let backendTower = this.game.towers[i];
	      let frontendTower = $($('ul')[i]);
	      backendTower.forEach((t) => {
	        let $li = $(`<li class='d${t}'></li>`);
	        frontendTower.prepend($li);
	      });
	      while (frontendTower.children().length < 3) {
	        let $li = $(`<li class='d0'></li>`);
	        frontendTower.prepend($li);
	      }
	      let $bar = $(`<li class='bar'></li>`);
	      frontendTower.append($bar);
	    }
	  }
	
	  getMove() {
	
	  }
	}
	
	module.exports = View;


/***/ }
/******/ ]);
//# sourceMappingURL=bundle.js.map