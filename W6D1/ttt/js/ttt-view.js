class View {
  constructor(game, $el) {
    this.game = game;
    this.$el = $el;
    this.setupBoard();
    this.bindEvents();
  }

  bindEvents() {
    $('li').on("click", (e) => {
      let cell = e.currentTarget;
      let $cell = $(cell);
      let strPos = $cell.attr("data-coord");
      let pos = [parseInt(strPos[1]), parseInt(strPos[3])];

      this.game.playMove(pos);
      $cell.append(this.game.currentPlayer);
      $cell.toggleClass("played");
      window.setTimeout(() => {if (this.game.isOver()) {
        if (this.game.winner()) {


          // $(".status").append(`Congrats ${this.game.winner()}!`);
          alert(`Congrats ${this.game.winner()}!`);
        } else {
          $(".status").append(`Tie game`);
        }
      }}, 50);

    });
  }

  makeMove($square) {

  }



  setupBoard() {

    const $ul = $("<ul></ul>");
    // debugger
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        $ul.append($(`<li data-coord='[${i},${j}]'></li>`));
      }
    }
    this.$el.append($ul);
  }
}

module.exports = View;
