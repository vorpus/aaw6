import React from 'react';
import ReactDOM from 'react-dom';

import Root from './root';
import Autocomplete from './autocomplete';


const main = document.getElementById('main');


ReactDOM.render(<Root />, main);
