import React from 'react';
import Root from './root';

class Weather extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      response: null
    };
    this.geoSuccess = this.geoSuccess.bind(this);
    this.geoError = this.geoError.bind(this);
    this.innerWeather = this.innerWeather.bind(this);
  }

  render () {
    return (
      <div className="weatherContainer">
      <h1>Weather</h1>
        <div className="weatherWidget group">
          {this.innerWeather()}
        </div>
      </div>
    );
  }

  innerWeather () {
    if (this.state.response) {
      let weatherDeets = JSON.parse(this.state.response);
      let temperature = Math.floor((weatherDeets.main.temp)*(9/5)-459.67);
      // console.log(weatherDeets);
      return (
        <div>
          <h2 className="weatherLocation">{weatherDeets.name}</h2>
          <div className="weatherTemperature">
            {temperature}° F
          </div>
        </div>
      );
    } else {
      return (
        <li>
          <h2>Loading...</h2>
        </li>
      );
    }
  }

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(this.geoSuccess, this.geoError);
  }

  geoSuccess(data) {
    let latitude = data.coords.latitude;
    let longitude = data.coords.longitude;

    let weatherWidget = this;

    const appid = 'bcb83c4b54aee8418983c2aff3073b3b';
    let weatherOptions = {
      dataType: 'json',
      method: 'GET',
      url: `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${appid}`,

    };

    const xhr = new XMLHttpRequest();
    xhr.open(weatherOptions.method, weatherOptions.url);

    xhr.onload = function () {
      weatherWidget.setState({response: xhr.response });
    };

    xhr.send(weatherOptions);
  }

  geoError(data) {
    console.log("error");
  }

}

export default Weather;
