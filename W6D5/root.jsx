import React from 'react';
import ReactDOM from 'react-dom';

import Tab from './tab';
import Autocomplete from './autocomplete';
import Weather from './weather';
import Clock from './clock';

class Root extends React.Component  {

  constructor(props) {
    super(props);

    this.state = {
      tabs: [{
        id: 0,
        title: 'one',
        content: 'onecontent'
      },
      {
        id: 1,
        title: 'two',
        content: 'twocontent'
      },
      {
        id: 2,
        title: 'red title',
        content: 'red content'
      }],
      names: ["alvin", "leonardo",
        "raphael", "michaelangelo",
        "donatello", "cece", "li", "pocpq","iecfa","lrahq","fwqml","yynpt",
        "hjjsm","evcju","lkjeu","hhlbn","crpzg","nxbpz","ikwzh","ycktp","lbrmx",
        "slvjv","tctfa","rjixs","fsxmy","vqmqf","tgslj","sjqqe","yszom","keuuh",
        "mpyve","vazxp","mnnzj","zvurg","etgws","eogwc","jenow","srcku","gcbmw",
        "twyfi","vonmt","qawmu","rsjtl","trnxj","wxvvy","khlbr","otvkw","whacu",
        "pigsw","efofp","ztyzf","uvqir","evnjj","yjplo","grfrl","lubht","elwkj",
        "qnfgm","nrvxs","zqnng","vawis","zmnce","zfaim","ljnte","solkt","wlwtz",
        "unhzg","lxala","nabrc","fhxex","nuhue","nwusa","nysfo","bygvl","coiyg",
        "lywmu","ulciz","lopls","uibvw","xwcui","rhsiy","vhrme","znpox","zuowz",
        "ninzy","jlvca","nymrm","oslcs","hvrsn","yhcmw","zpubk","mzcoh","sxijy",
        "kppkt","rgzzm","clpxe","swrpj","nkbln","nigny","iapgg","ngznt","krvxc",
        "kolej","kixtw","wfyrc","llbvj","mhqby","eyeeh","xhmop","yamgj","twzsr",
        "vrhnf","quahu","vryxq","rxsuf","omtjb","jazvf","exuhi","rzkly","aucqu",
        "zhgnl","qqtto","wnvqm","gvaab","vpbom","attqm","atjsb","scxqr","fquun",
        "aazff","fsgfu","vxizm","bbvrx","wqywa","ncnan","gbsyb","zbpgk","gzgwl",
        "pvjhe","uzmay","caiob","tsyiz","cualu","koujq","vrkgh","rmaau","xnssr",
        "cbpmi","tbkbj","qrlws","yjrik","wcwxl","tobtr","ipmkg","yfxow","nvjey",
        "qxigw","lwsim","wvpaj","rnqcs","voclo","olngc","rlilo","fjilm","gyfmm",
        "gkllq","yyssc","auyyk","xjmgl","hymlo","wglqp","ysxwl","tafyu","qbjcy",
        "vikvw","agcii","oluef","bkcpk","ypfky","irujz","pvfph","uzkae","suwce",
        "hubgw","wtnsc","fqoxx","imkip","jmlix","qtbnq","oriop","tlqnf","ovxci",
        "frvzs","eutes","eckzn","sefoz","epuqt","gvxaf","tlqyn","aebvf","hvaas",
        "qxjeq","nkxaf","lbfxg","kzwcv","uutrj","urnhy"]
    };

  }

  render(){
    return (
      <main>
        <Tab tabs= { this.state.tabs } />
        <Autocomplete names={this.state.names} />
        <Weather />
        <Clock />
      </main>
    );
  }
}

export default Root;
