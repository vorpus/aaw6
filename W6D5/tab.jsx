import React from 'react';

import Root from './root';

class Tab extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selectedTab: 0 };
  }

  render() {
    const tabItems = this.props.tabs.map((tab) => {
      return (
        <li key={tab.id}>
          <h3 key={tab.id} id={tab.id} onClick={this.update.bind(this)}>{ tab.title }</h3>
        </li>
      );
    });



    return (
      <div className="tabs group">
      <h1>Tabs</h1>
        <ul className="group">
          { tabItems }
        </ul>

        <article className="t-article">{ this.tabArticle.call(this) }</article>
      </div>
    );
  }


  tabArticle() {
    for (let i = 0; i < this.props.tabs.length; i++) {
      if (this.state.selectedTab === this.props.tabs[i].id) {
        return (
          this.props.tabs[i].content
        );
      }
    }
  }

  update(e) {
    e.preventDefault();
    // debugger
    this.setState({ selectedTab: parseInt(e.currentTarget.id) });
    // console.log(this.state.selectedTab);
  }
}

export default Tab;
