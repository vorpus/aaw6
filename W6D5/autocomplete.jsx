import React from 'react';
import Root from './root';

class Autocomplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = { displayNames: this.props.names, searchedName: "" };
  }

  render() {
    const allNames = this.state.displayNames.slice(0,5).map((name) => {
      return (
        <li key={name}>{name}</li>
      );
    });

    return (
      <div className="searchBoxer">
      <h1>Search</h1>
        <div className="search">

          <input onChange={this.returnResults.bind(this)}></input>
          <ul>
          {allNames}
          </ul>
        </div>
      </div>
    );
  }

  returnResults(e) {
    this.setState({searchedName: e.target.value});
    let pattern = new RegExp(`^${this.state.searchedName}`);
    setTimeout(this.setState({displayNames: this.filterNames(pattern)}), 100);
  }

  filterNames(pattern) {
    // debugger
    let filteredName = [];
    this.props.names.forEach((name)=> {
      if (name.match(pattern) !== null ) {
        filteredName.push(name);
      }
    });
    return filteredName;
  }

  // console.log(this.state.searchedName);
}

export default Autocomplete;
