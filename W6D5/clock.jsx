import React from 'react';
import Root from './root';

class Clock extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      time: null
    };
    this.time = new Date();
    this.state.hours = this.time.getHours();
    this.state.minutes = this.time.getMinutes();
    this.state.seconds = this.time.getSeconds();

    this.incrementSecond = this.incrementSecond.bind(this);
  }

  render () {

    return (
      <div className="clockContainer">
        <h1>Clock</h1>
        <div className="clockWidget group">
          <li>{this.state.hours}:{this.state.minutes}:{this.state.seconds}</li>
        </div>
      </div>
    );
  }

  incrementSecond () {
    console.log('incrementing');
    let hours = this.state.hours;
    let minutes = this.state.minutes;
    let seconds = this.state.seconds;

    seconds++;
    if (seconds === 60) {
      seconds = 0;
      minutes++;
    }
    if (minutes === 60) {
      minutes = 0;
      hours++;
    }
    if (hours == 24) {
      hours = 0;
    }

    this.setState({
      hours: hours,
      minutes: minutes,
      seconds: seconds
    });
  }

  componentDidMount() {
    this.timer = setInterval(this.incrementSecond, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }
}

export default Clock;
